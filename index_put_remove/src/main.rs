use clap::{Args, Parser, Subcommand};
use persy::{inspect::PersyInspect, ByteVec, Config, Persy, PersyError, TransactionConfig, ValueMode};
use std::error::Error;

use common_telemetry::init_tracing_otel;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use tracing::{info, instrument, trace};

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    #[command(subcommand)]
    commands: Commands,
}
#[derive(Debug, Subcommand)]
enum Commands {
    Check { path: String },
    Run(RunCfg),
}

#[derive(Debug, Args)]
pub struct RunCfg {
    path: String,
    #[arg(short, default_value_t = false)]
    background: bool,
}

fn main() -> Result<(), Box<dyn Error>> {
    let _otel_active = init_tracing_otel("index_put_remove")?;
    let args = Cli::parse();
    match &args.commands {
        Commands::Check { path } => {
            check(path)?;
        }
        Commands::Run(run_cfg) => {
            run(&run_cfg)?;
        }
    };

    Ok(())
}

#[instrument(level = "info")]
pub fn run(run: &RunCfg) -> Result<(), Box<dyn Error>> {
    let mut p = std::path::PathBuf::from(&run.path);
    p.push("data.persy");
    let mut config = Config::new();
    config.change_cache_size(1024 * 1024 * 256);
    let persy = Persy::open_or_create_with(p, config, |_p| Ok(()))?;

    let finished = Arc::new(AtomicUsize::new(10));
    let atomic = Arc::new(AtomicUsize::new(0));
    if !persy.exists_index("test")? {
        let mut tx = persy.begin()?;

        tx.create_index::<ByteVec, ByteVec>("test", ValueMode::Replace)?;

        let prepared = tx.prepare()?;

        prepared.commit()?;
    }

    let reader = atomic.clone();
    let fin = finished.clone();
    std::thread::spawn(move || loop {
        std::thread::sleep(std::time::Duration::from_millis(1000));

        println!("{} recs/second", reader.load(Ordering::SeqCst));
        reader.store(0, Ordering::SeqCst);
        if fin.load(Ordering::SeqCst) == 0 {
            break;
        }
    });

    let mut handles: Vec<std::thread::JoinHandle<Result<(), PersyError>>> = vec![];
    for t in 0..30 {
        let db = persy.clone();
        let atomic_cloned = atomic.clone();
        let fin = finished.clone();
        let thread_cfg = ThreadConfig {
            thread: t,
            background: run.background,
        };
        handles.push(
            std::thread::Builder::new()
                .spawn(move || {
                    single_thread_operation(db, thread_cfg, atomic_cloned, fin)?;
                    Ok(())
                })
                .unwrap(),
        )
    }

    for t in handles {
        t.join().unwrap()?;
    }

    Ok(())
}

#[derive(Clone, Debug)]
struct ThreadConfig {
    thread: usize,
    background: bool,
}

#[instrument(level = "info", skip(db, atomic_cloned, fin))]
fn single_thread_operation(
    db: Persy,
    cfg: ThreadConfig,
    atomic_cloned: Arc<AtomicUsize>,
    fin: Arc<AtomicUsize>,
) -> Result<(), PersyError> {
    info!(" started {:?}", std::thread::current().id());
    let mut tx = db
        .begin_with(TransactionConfig::new().set_background_sync(cfg.background))
        .map_err(|e| e.error())?;
    for iteration in 0..100 {
        trace!("iteration: {iteration}");
        for n in 0..1000 {
            let kv = ByteVec::from(format!("{}+{}", cfg.thread, n).as_bytes());
            tx.put("test", kv.clone(), kv).map_err(|e| e.error())?;
            if n % 100 == 0 {
                let prepared = tx.prepare().map_err(|e| e.error())?;
                prepared.commit().map_err(|e| e.error())?;
                tx = db
                    .begin_with(TransactionConfig::new().set_background_sync(cfg.background))
                    .map_err(|e| e.error())?;
            }
            atomic_cloned.fetch_add(1, Ordering::SeqCst);
        }

        for n in 0..1000 {
            let kv = ByteVec::from(format!("{}+{}", cfg.thread, n).as_bytes());
            tx.remove("test", kv.clone(), Some(kv)).map_err(|e| e.error())?;
            if n % 100 == 0 {
                let prepared = tx.prepare().map_err(|e| e.error())?;
                prepared.commit().map_err(|e| e.error())?;
                tx = db
                    .begin_with(TransactionConfig::new().set_background_sync(cfg.background))
                    .map_err(|e| e.error())?;
            }
            atomic_cloned.fetch_add(1, Ordering::SeqCst);
        }
    }
    info!(" finished {:?}", std::thread::current().id());
    fin.fetch_sub(1, Ordering::SeqCst);

    Ok(())
}

#[instrument(level = "info")]
pub fn check(path: &str) -> Result<(), Box<dyn Error>> {
    use persy::inspect::PrintTreeInspector;
    use persy_inspect_index::{CheckPrintTreeInspector, FailCheckResults};
    let mut p = std::path::PathBuf::from(path);
    p.push("data.persy");
    let mut config = Config::new();
    config.change_cache_size(1024 * 1024 * 256);
    let persy = Persy::open_or_create_with(p, config, |_p| Ok(()))?;
    persy
        .inspect_tree::<ByteVec, ByteVec, _>("test", &mut PrintTreeInspector::new())
        .map_err(|e| e.error())?;
    persy
        .inspect_tree::<ByteVec, ByteVec, _>("test", &mut CheckPrintTreeInspector::new(Box::new(FailCheckResults())))
        .map_err(|e| e.error())?;
    Ok(())
}
