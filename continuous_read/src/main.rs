use clap::{Parser, Subcommand};
use common_telemetry::init_tracing_otel;
use memory_stats::memory_stats;
use persy::{Config, Persy, PersyId, ValueMode};
use std::error::Error;
use std::path::Path;

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    #[command(subcommand)]
    commands: Commands,
}
#[derive(Debug, Subcommand)]
enum Commands {
    Check { path: String },
    Run { path: String },
}

fn main() -> Result<(), Box<dyn Error>> {
    let _otel_active = init_tracing_otel("index_put_remove")?;
    let args = Cli::parse();
    match &args.commands {
        Commands::Check { path } => {
            run(path, true)?;
        }
        Commands::Run { path } => {
            run(&path, false)?;
        }
    };

    Ok(())
}

fn run(path: &str, check: bool) -> Result<(), Box<dyn Error>> {
    let data_dir = Path::new(path);
    let file_path = data_dir.join("data.persy");
    let exists = file_path.exists();
    if !check && !exists {
        Persy::create(&file_path).unwrap();
    }
    let db = Persy::open(file_path, Config::default()).unwrap();

    if !check {
        if !exists {
            let mut tx = db.begin().unwrap();
            tx.create_segment("data").unwrap();
            tx.create_index::<u64, PersyId>("primary", ValueMode::Replace).unwrap();
            let prepared = tx.prepare().unwrap();
            prepared.commit().unwrap();
        }

        let mut tx = db.begin().unwrap();

        for x in 0..1_000 {
            let id = tx
                .insert("data", b"seaftsetbtsewwbtsebtewabtsewewbtaybdysbrbyseryebraewbrssbew")
                .unwrap();

            tx.put::<u64, PersyId>("primary", x, id).unwrap();
        }

        let prepared = tx.prepare().unwrap();
        prepared.commit().unwrap();
    }
    let pre = memory_stats().unwrap().physical_mem;
    let limit = pre * 2;

    let k = 157;

    for c in 0..1_000_000 {
        let id = db.get::<u64, PersyId>("primary", &k).unwrap().next().unwrap();
        db.read("data", &id).unwrap().unwrap();
        if c % 1000 == 0 {
            let current_memory = memory_stats().unwrap().physical_mem;

            if limit < current_memory {
                return Err(format!("Error memory overgrown limit:{} value:{}", limit, current_memory).into());
            }
        }
    }
    Ok(())
}
