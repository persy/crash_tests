use chrono::Duration;
use clap::Parser;
use fs_extra::dir::{copy, CopyOptions};
use serde::Deserialize;
use std::{
    error::Error,
    fmt::{Debug, Display},
    fs::{create_dir_all, read_to_string, OpenOptions},
    path::PathBuf,
    process::{Child, Command},
    sync::{Arc, Condvar, Mutex},
};
use threadpool::ThreadPool;
use timer::Timer;
use tracing::{error, info, instrument, trace};

#[derive(Deserialize)]
struct Config {
    tests: Vec<Test>,
    working_dir: Option<String>,
}

#[derive(Deserialize, Clone)]
struct Test {
    command: String,
    name: String,
    iterations: u32,
    kill_timeout: u32,
    envs: Vec<Env>,
    #[serde(default)]
    run_args: Vec<Arg>,
}
impl Display for Test {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl Debug for Test {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Deserialize, Clone)]
struct Env {
    name: String,
    value: String,
}

#[derive(Deserialize, Clone)]
struct Arg {
    name: String,
    value: Option<String>,
}

#[derive(Clone, Debug)]
struct TestPaths {
    working_dir: PathBuf,
    log_dir: PathBuf,
    backup_dir: PathBuf,
}

type Context = Arc<ContextMutex>;

struct ContextMutex {
    context: Mutex<ContextImpl>,
    cond: Condvar,
}
impl ContextMutex {
    fn new(config: &Config) -> Result<Self, Box<dyn Error>> {
        Ok(Self {
            context: Mutex::new(ContextImpl::new(config)?),
            cond: Condvar::new(),
        })
    }

    fn test_paths(&self, test: &Test, iteration: u32) -> Result<TestPaths, Box<dyn Error>> {
        self.context.lock().unwrap().test_paths(test, iteration)
    }

    fn schedule<F>(&self, timeout: u32, f: F) -> Result<(), Box<dyn Error>>
    where
        F: FnOnce() -> () + std::marker::Send + 'static,
    {
        self.context.lock().unwrap().schedule(timeout, f)
    }

    fn start(&self, test: &Test) -> Result<(), Box<dyn Error>> {
        self.context.lock().unwrap().start(test)
    }
    fn failed(&self, test: &Test, paths: &TestPaths) -> Result<(), Box<dyn Error>> {
        let mut lock = self.context.lock().unwrap();
        if lock.failed(test, paths)? {
            self.cond.notify_all();
        }
        Ok(())
    }
    fn finished(&self, test: &Test, paths: &TestPaths) -> Result<(), Box<dyn Error>> {
        let mut lock = self.context.lock().unwrap();
        if lock.finished(test, paths)? {
            self.cond.notify_all();
        }
        Ok(())
    }
    fn wait_finish(&self) -> Result<(), Box<dyn Error>> {
        let lock = self.context.lock().unwrap();
        if lock.wait_finish()? {
            let _ = self.cond.wait(lock);
        }
        Ok(())
    }
}

struct ContextImpl {
    working_dir: PathBuf,
    timer: Timer,
    thread_pool: Arc<Mutex<ThreadPool>>,
    runing_count: u32,
}
impl ContextImpl {
    fn new(config: &Config) -> Result<Self, Box<dyn Error>> {
        let working_dir = check_and_create_working_dir(config.working_dir.as_ref().map(|s| s.as_str()))?;
        Ok(Self {
            working_dir,
            timer: Timer::new(),
            thread_pool: Arc::new(Mutex::new(ThreadPool::new(10))),
            runing_count: 0,
        })
    }

    fn test_paths(&mut self, test: &Test, iteration: u32) -> Result<TestPaths, Box<dyn Error>> {
        let name = test.name.clone();
        let run = format!("{}", iteration);
        let mut test_path = self.working_dir.canonicalize()?;
        test_path.push(name);
        create_dir_all(&test_path)?;
        let mut working_dir = test_path.clone();
        working_dir.push("work");
        trace!("creating {}", working_dir.to_string_lossy());
        create_dir_all(&working_dir)?;

        let mut log_dir = test_path.clone();
        log_dir.push("log");
        log_dir.push(&run);
        trace!("creating {}", log_dir.to_string_lossy());
        create_dir_all(&log_dir)?;

        let mut backup_dir = test_path.clone();
        backup_dir.push("backup");
        backup_dir.push(run);
        trace!("creating {}", backup_dir.to_string_lossy());
        create_dir_all(&backup_dir)?;
        Ok(TestPaths {
            working_dir,
            log_dir,
            backup_dir,
        })
    }

    fn schedule<F>(&self, timeout: u32, f: F) -> Result<(), Box<dyn Error>>
    where
        F: FnOnce() -> () + std::marker::Send + 'static,
    {
        let mut fun = Some(Some(f));
        let tp = self.thread_pool.clone();
        self.timer
            .schedule_with_delay(Duration::milliseconds(timeout as i64), move || {
                if let Some(mut ff) = fun.take() {
                    tp.lock().unwrap().execute(move || {
                        if let Some(f) = ff.take() {
                            f();
                        }
                    });
                }
            })
            .ignore();
        Ok(())
    }

    fn start(&mut self, test: &Test) -> Result<(), Box<dyn Error>> {
        info!("starting {} ", test.name,);
        self.runing_count += 1;
        Ok(())
    }

    fn failed(&mut self, test: &Test, paths: &TestPaths) -> Result<bool, Box<dyn Error>> {
        error!(
            "failed run {} logs at {} backup at {}",
            test.name,
            paths.log_dir.to_string_lossy(),
            paths.backup_dir.to_string_lossy()
        );
        self.runing_count -= 1;
        Ok(self.runing_count == 0)
    }

    fn finished(&mut self, test: &Test, paths: &TestPaths) -> Result<bool, Box<dyn Error>> {
        info!(
            "finished successfully {} working at {}",
            test.name,
            paths.working_dir.to_string_lossy()
        );
        self.runing_count -= 1;
        Ok(self.runing_count == 0)
    }
    fn wait_finish(&self) -> Result<bool, Box<dyn Error>> {
        Ok(true)
    }
}

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    config: String,
}
#[instrument(level = "info", skip(context))]
fn execute_test(test: &Test, context: Context) -> Result<(), Box<dyn Error>> {
    context.start(test)?;
    run_test(test, context, 0)?;
    Ok(())
}

#[instrument(level = "debug", skip(context))]
fn run_test(test: &Test, context: Context, iteration: u32) -> Result<(), Box<dyn Error>> {
    let paths = context.test_paths(test, iteration)?;
    let mut out_file = paths.log_dir.clone();
    out_file.push("out");

    let mut err_file = paths.log_dir.clone();
    err_file.push("err");
    let command = PathBuf::from(&test.command);

    info!("running {} iteration {}", test.name, iteration);
    trace!(
        "running {} cmd={} out={} err={} ",
        test.name,
        command.to_string_lossy(),
        out_file.to_string_lossy(),
        err_file.to_string_lossy()
    );

    let out = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open(out_file)?;
    let err = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open(err_file)?;
    let mut base = Command::new(&command);
    let mut args = base.current_dir("./").arg("run").arg(&paths.working_dir);
    for arg in &test.run_args {
        args = args.arg(&arg.name);
        if let Some(value) = &arg.value {
            args = args.arg(value);
        }
    }
    let running = args.stdout(out).stderr(err).spawn()?;
    trace!("started {}", test.name);
    let tt = test.clone();
    let pp = paths.clone();
    let c = context.clone();

    context.schedule(test.kill_timeout, move || {
        kill_and_check(running, tt, pp, c, iteration).unwrap();
    })?;
    Ok(())
}

#[instrument(level = "debug", skip(context))]
fn run_check(test: &Test, paths: &TestPaths, context: Context) -> Result<bool, Box<dyn Error>> {
    trace!("checking {}", test.name);
    let mut out_file = paths.log_dir.clone();
    out_file.push("out");

    let mut err_file = paths.log_dir.clone();
    err_file.push("err");

    trace!(
        "checking {} out={} err={} ",
        test.command,
        out_file.to_string_lossy(),
        err_file.to_string_lossy()
    );

    let out = OpenOptions::new().write(true).append(true).open(out_file)?;
    let err = OpenOptions::new().write(true).append(true).open(err_file)?;
    let mut running = Command::new(&test.command);
    let mut running = running.arg("check").arg(&paths.working_dir).stdout(out).stderr(err);
    for env in &test.envs {
        running = running.env(&env.name, &env.value);
    }

    let mut running = running.spawn()?;
    let status = running.wait()?;
    if !status.success() {
        context.failed(test, paths)?;
    }
    Ok(status.success())
}
fn backup(_test: &Test, paths: &TestPaths) -> Result<(), Box<dyn Error>> {
    copy(&paths.working_dir, &paths.backup_dir, &CopyOptions::new())?;
    Ok(())
}
fn kill_and_check(
    mut child: Child,
    test: Test,
    paths: TestPaths,
    context: Context,
    iteration: u32,
) -> Result<(), Box<dyn Error>> {
    trace!("killing {}", test.name);
    child.kill()?;
    backup(&test, &paths)?;
    let success = run_check(&test, &paths, context.clone())?;
    if success {
        if iteration < test.iterations {
            run_test(&test, context, iteration + 1)?;
        } else {
            context.finished(&test, &paths)?;
        }
    }
    Ok(())
}

fn check_and_create_working_dir(path: Option<&str>) -> Result<PathBuf, Box<dyn Error>> {
    let path = path.unwrap_or("./work");
    let path = PathBuf::from(path);
    create_dir_all(&path)?;
    Ok(path)
}

#[instrument(level = "info", skip(context, config))]
fn execute(config: &Config, context: Context) -> Result<(), Box<dyn Error>> {
    for test in &config.tests {
        execute_test(test, context.clone())?;
    }
    context.wait_finish()?;
    info!("All tests finished");
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    tracing_subscriber::fmt::init();
    let args = Cli::parse();
    let config: Config = toml::from_str(&read_to_string(&args.config)?)?;

    let context = Arc::new(ContextMutex::new(&config)?);
    execute(&config, context)?;
    Ok(())
}
