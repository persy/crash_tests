use tracing_subscriber::prelude::*;
use tracing_subscriber::{fmt, registry, EnvFilter};

use opentelemetry::{
    sdk::{
        propagation::TraceContextPropagator,
        trace::{config, RandomIdGenerator, Sampler},
        Resource,
    },
    KeyValue,
};
use opentelemetry_otlp::{new_exporter, new_pipeline, WithExportConfig};

pub struct DeInit {}
impl Drop for DeInit {
    fn drop(&mut self) {
        opentelemetry::global::shutdown_tracer_provider();
    }
}

pub fn init_tracing_otel(name: &str) -> Result<DeInit, Box<dyn std::error::Error>> {
    let end_point = std::env::var("OTEL_ENDPOINT").unwrap_or("http://127.0.0.1:4318/v1/traces".to_owned());

    opentelemetry::global::set_text_map_propagator(TraceContextPropagator::new());
    let tracer = new_pipeline()
        .tracing()
        .with_exporter(new_exporter().http().with_endpoint(end_point))
        .with_trace_config(
            config()
                .with_sampler(Sampler::AlwaysOn)
                .with_id_generator(RandomIdGenerator::default())
                .with_resource(Resource::new(vec![KeyValue::new("service.name", name.to_owned())])),
        )
        .install_simple()?;
    let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    let fmt_layer = fmt::layer().with_target(false);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    registry().with(filter_layer).with(opentelemetry).with(fmt_layer).init();
    Ok(DeInit {})
}
